<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link href="styles/style.css" rel="stylesheet">
    <script src="scripts/jquery-3.4.1.min.js"></script>
    <script src="scripts/main.js"></script>
    <title>Онлайн магазин</title>
</head>
<body>
<header>
    <div id="headerTop">
        <a href="index.php" ><div id="logo"></div></a>
        <div id="companyName">{$sitename}</div>
        <div id="navWrap">
            <a href="index.php">
                Главная
            </a>
            <a href="index.php?page=shop">
                Магазин
            </a>
            <a href="index.php?page=info">
                О нас
            </a>
        </div>
    </div>
</header>

<div id="content">
    {if !isset($smarty.get.page)}
        {include 'main.tpl'}
        {elseif ($smarty.get.page == 'shop')}
        {include 'shop.tpl'}
        {elseif ($smarty.get.page == 'product')}
        {include 'openedProduct.tpl'}
        {elseif ($smarty.get.page == 'info')}
        {include 'info.tpl'}
    {/if}
</div>

<footer>
    <div id="footerInside">

        <div id="contacts">
            <div class="contacts">
                <img src="images/envelope.svg" class="contactIcon" alt="">
                zamkoved73@info.com
            </div>
            <div class="contacts">
                <img src="images/phone-call.svg" class="contactIcon" alt="">
                +7 (999) 357 32-12
            </div>
            <div class="contacts">
                <img src="images/placeholder.svg" class="contactIcon" alt="">
                Ульяновск, ​Академика Филатова проспект, 12а
            </div>
        </div>

        <div id="footerNav">
            <a href="index.php">Главная</a>
            <a href="index.php?page=shop">Магазин</a>
        </div>

        <div id="social">
            <img class="socialItem" src="images/vk-social-logotype.svg" alt="">
            <img class="socialItem" src="images/google-plus-social-logotype.svg" alt="">
            <img class="socialItem" src="images/facebook-logo.svg" alt="">
        </div>

        <div id="copyrights">&copy; Zamkoved</div>
    </div>
</footer>

</body>
</html>