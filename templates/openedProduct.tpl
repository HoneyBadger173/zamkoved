{$id = $smarty.get.id}
{foreach $goods as $product}
    {if $product.id == $id}
        {$good = $product}
<div id="openedProduct-img">
    <img src="{$good.img}" alt="">
</div>
<div id="openedProduct-content">
    <h1 id="openedProduct-name">
        {$good.name}
    </h1>
    <div id="openedProduct-desc">
        {$good.desc}
    </div>
    <div id="openedProduct-price">
        Цена: {$good.price}
    </div>
</div>
{/if}
{/foreach}
<div id="backButton">
    <a href="#" onclick="history.back();return false;">Вернуться назад</a>
</div>