<h1 align="center">
    Каталог товаров
</h1>

<div>
    {foreach $goods as $good}
        <div class="shopUnit">
            <img src='{$good.img}' alt="">

            <div class="shopUnitName">
                {$good.name}
            </div>
            <div class="shopUnitShortDesc">
                {$good.desc}
            </div>
            <div class="shopUnitPrice">
                Цена: {$good.price}
            </div>
            <a href="index.php?page=product&id={$good.id}" class="shopUnitMore">
                Подробнее
            </a>
        </div>
    {/foreach}
</div>

<div class="pager">
    {foreach $pagination as $page}
            <span class="pagination_page {if $_REQUEST.current_page == $page}active_page{/if}">{$page}</span>
    {/foreach}
</div>

<div id="backss">
    <a href="/">На главную</a>
</div>



<script>
    //$(document).ready(function() {
        $('.pagination_page').click(function(e) {
            $.ajax({
                method: "GET",
                url: "index.php?page=shop",
                data: {
                    "current_page": $(this).text()
                },
                success: function (data) {

                    console.log("SUCCESS: ", data);
                    $('body').html(data);
                },
                error: function (e) {

                    console.log("ERROR: ", e);
                    $('body').html(e);
                }
            });
        });
    //});
</script>