<?php

require_once('libs/Smarty.class.php');

$smarty = new Smarty();

$goods = array(
        1 => array(
                array(
                    'id' => 1,
                    'name' => 'Apex Premier',
                    'desc' => 'Замок врезной Apecs Premier T-52-CR хром',
                    'img' => 'images/goods/apex-premier.jpg',
                    'price' => '472 руб./шт.'
                ),
                array(
                    'id' => 2,
                    'name' => 'для кит.метал.дверей',
                    'desc' => 'Замок врезной б/руч СТАНДАРТ ЗВ1-09СТ',
                    'img' => 'images/goods/bruch.jpg',
                    'price' => '365 руб./шт.'
                ),
                array(
                    'id' => 3,
                    'name' => 'Замок накладной Зенит',
                    'desc' => 'Замок накладной Зенит-ЗН-1-3 (бронза)',
                    'img' => 'images/goods/zenit.jpg',
                    'price' => '495 руб./шт.'
                )
        ),
        2 => array(
                array(
                    'id' => 4,
                    'name' => 'Замок навесной АВАНГАРД',
                    'desc' => 'Замок навесной АВАНГАРД ВС2М-60 Медведь Бронза d11,5мм',
                    'img' => 'images/goods/avangard.jpg',
                    'price' => '545 руб./шт.'
                ),
                array(
                    'id' => 5,
                    'name' => 'Замок врезной ЗВ4',
                    'desc' => 'Замок врезной ЗВ4 713.0.0 МЕТТЭМ',
                    'img' => 'images/goods/zv4.jpg',
                    'price' => '582 руб./шт.'
                ),
                array(
                    'id' => 6,
                    'name' => 'Замок врезной Avers',
                    'desc' => 'Замок врезной Avers 0823/60-C-AC медь',
                    'img' => 'images/goods/avers.jpg',
                    'price' => '795 руб./шт.'
                )
        ),
        3 => array(
                array(
                    'id' => 7,
                    'name' => 'Замок врезной Avers',
                    'desc' => 'Замок врезной Avers 0823/60-C-AC медь',
                    'img' => 'images/goods/avers.jpg',
                    'price' => '795 руб./шт.'
                ),
                array(
                    'id' => 8,
                    'name' => 'Замок врезной Avers',
                    'desc' => 'Замок врезной Avers 0823/60-C-AC медь',
                    'img' => 'images/goods/avers.jpg',
                    'price' => '795 руб./шт.'
                ),
                array(
                    'id' => 9,
                    'name' => 'Замок врезной Avers',
                    'desc' => 'Замок врезной Avers 0823/60-C-AC медь',
                    'img' => 'images/goods/avers.jpg',
                    'price' => '795 руб./шт.'
                )
        ),
        4 => array(
                array(
                    'id' => 10,
                    'name' => 'Замок врезной Avers',
                    'desc' => 'Замок врезной Avers 0823/60-C-AC медь',
                    'img' => 'images/goods/avers.jpg',
                    'price' => '795 руб./шт.'
                ),
                array(
                    'id' => 11,
                    'name' => 'Замок врезной Avers',
                    'desc' => 'Замок врезной Avers 0823/60-C-AC медь',
                    'img' => 'images/goods/avers.jpg',
                    'price' => '795 руб./шт.'
                ),
                array(
                    'id' => 12,
                    'name' => 'Замок врезной Avers',
                    'desc' => 'Замок врезной Avers 0823/60-C-AC медь',
                    'img' => 'images/goods/avers.jpg',
                    'price' => '795 руб./шт.'
                )
        ),
        5 => array(
                array(
                    'id' => 13,
                    'name' => 'Замок врезной Avers',
                    'desc' => 'Замок врезной Avers 0823/60-C-AC медь',
                    'img' => 'images/goods/avers.jpg',
                    'price' => '795 руб./шт.'
                )
        )
);

$smarty->assign('goods', !empty($_REQUEST['current_page']) ? $goods[$_REQUEST['current_page']] : $goods[1]);
$smarty->assign('pagination', range(1, count($goods)));

$smarty->assign('sitename', 'Zamkoved');
$smarty->display('index.tpl')
?>
